package com.example.practica02imckotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    // Declaración de objetos del MainActivity
    private lateinit var txtAlturaMain: EditText
    private lateinit var txtPesoMain: EditText
    private lateinit var lblIMCMain: TextView
    private lateinit var btnCalcularMain: Button
    private lateinit var btnLimpiarMain: Button
    private lateinit var btnCerrarMain: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relación de objetos
        txtAlturaMain = findViewById(R.id.txtAltura)
        txtPesoMain = findViewById(R.id.txtPeso)
        lblIMCMain = findViewById(R.id.lblIMC)
        btnCalcularMain = findViewById(R.id.btnCalcular)
        btnLimpiarMain = findViewById(R.id.btnLimpiar)
        btnCerrarMain = findViewById(R.id.btnCerrar)

        // Codificación del botón para calcular el IMC
        btnCalcularMain.setOnClickListener {
            // Validar que los campos no estén vacíos
            if(txtAlturaMain.text.toString().isEmpty()) {
                txtAlturaMain.setError("Campo requerido")
            } else if(txtPesoMain.text.toString().isEmpty()) {
                txtPesoMain.setError("Campo requerido")
            } else {
                // Tomar los valores de peso y altura como tipo Double
                val altura = txtAlturaMain.text.toString().toDouble()
                val peso = txtPesoMain.text.toString().toDouble()

                // Validar que los valores no sean igual a 0
                if(altura != 0.0 || peso != 0.0) {
                    // Cálculo de IMC (peso en kilogramos dividido por altura en metros al cuadrado)
                    // Se convierten los centímetros a metros
                    val alturaM = altura / 100
                    val imc = peso / (alturaM * alturaM)

                    // Definir formato de 2 decimales
                    val df = DecimalFormat("#.00")

                    // Mostrar el resultado del cálculo del IMC
                    lblIMCMain.text = ("Su IMC es: ${df.format(imc)} kg/m2")
                }
            }
        }

        // Codificación del botón para limpiar los EditText y el TextView del IMC
        btnLimpiarMain.setOnClickListener {
            // Limpiar las cajas de texto y el resultado del IMC
            // Validar si los campos están vacíos
            if(txtAlturaMain.text.toString().isEmpty() && txtPesoMain.text.toString().isEmpty() &&
                    lblIMCMain.text.toString() == "Su IMC es:"){
                    Toast.makeText(this,"No hay información para limpiar",
                        Toast.LENGTH_SHORT).show()
            } else {
                txtAlturaMain.setText("")
                txtPesoMain.setText("")
                lblIMCMain.text = "Su IMC es:"
            }
        }

        // Codificación del botón para cerrar la aplicación
        btnCerrarMain.setOnClickListener {
            finish()
        }
    }
}